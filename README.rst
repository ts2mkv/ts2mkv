About
-----
Ts2mkv is a command line utility to convert `DVB MPEG-2 Transport Stream <http://en.wikipedia.org/wiki/MPEG_transport_stream>`_ (.ts) files into `Matroska <http://matroska.org/>`_ (.mkv) which is well supported by all modern media players and devices. The conversion process can also have a separate encoding phase which radically reduces the resulting mkv file size. Ts2mkv comes with some useful presets for encoding video tracks into H.264 and audio tracks into AAC, AC3 or MP3.

Ts2mkv uses `avconv <http://libav.org/avconv.html>`_, `ProjectX <http://project-x.sourceforge.net/>`_ and `MKVToolNix <https://www.bunkus.org/videotools/mkvtoolnix>`_ via their command line interfaces. Tweaked version of ProjectX is bundled with ts2mkv setup.

Ts2mkv comes with pre-defined configurations for Finnish channels' subtitles. (DVB subtitles work in `YLE <http://yle.fi/>`_ and `AVA <http://www.avatv.fi/>`_ channels. Teletext subtitles also work in YLE channels if available.)

Usage
-----
Basic usage is::

  ts2mkv my_recording.ts

You can also enable video and audio track encoding by adding some additional parameters::

  ts2mkv my_recording.ts --video-preset=h264 --audio-preset=aac

For full syntax type::

  ts2mkv --help

Mkvenc
------
Mkvenc is a supplementary tool bundled with ts2mkv. It can be used to apply the encoding phase afterwards if the original ts->mkv conversion was made without encoding. Basically the tool can be used to encode any Matroska file but it depends on codecs, etc.

In the first phase mkvenc extracts all the tracks out from the existing Matroska file. Then the video and audio tracks are encoded based on presets and configuration. Finally the tracks are merged back together in single Matroska file. Note: Mkvenc tries to preserve language information and possible chapters but some information may be lost during the process.
